Read Me Please for getting the PHPUnit Test repo:

wget https://phar.phpunit.de/phpunit.phar

➜ chmod +x phpunit.phar

➜ sudo mv phpunit.phar /usr/local/bin/phpunit

➜ phpunit --version
PHPUnit 4.7.0 by Sebastian Bergmann and contributors.