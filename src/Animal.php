<?php
namespace Being\Animal;


require_once 'Being.php';



/**
 * Abstract Class Animal that extends another abstract class Being
 */
abstract class Animal extends \Being\Being{
	protected $age;
	
        /**
         * A constructor
         * @param type $a
         */
	function __construct($a){
		$this->age = $a;
        }
	
        /**
         * Get Age
         * @return type Integer
         */
	public function getAge(){
		return $this->age;
	}
}