<?php
namespace Being\Animal\Cat;
include_once 'Animal.php';


/**
 * Cat Class extending Animal Class
 */
class Cat extends \Being\Animal\Animal{
	private $name;
	
        /**
         * A constructor
         * @param type $n
         * @param type $a
         */
	function __construct($n,$a){
		$this->name=$n;
		parent::__construct($a);
	}
	
        /**
         * Get Name
         * @return type String
         */
	public function getName(){
		return $this->name;
	}
        
        /**
         * Can Walk
         * 
         */
        public function canWalk(){
            if ( parent::isAlive())
                return $this->name . " can walk!<br/>";
            else
                return $this->name . " cannot walk!<br/>";
        }
        
        /**
         * Can Run
         * 
         */
        public function canRun(){
            if ( parent::isAlive())
                return $this->name . " can run!<br/>";
            else
                return $this->name . " cannot run!<br/>";
        }
        

}


