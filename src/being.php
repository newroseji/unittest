<?php
namespace Being;

/**
 * An Interface for Being
 * 
 */
interface iMovement{
    public function canWalk();
    public function canRun();
}

/**
 * Abstract Class Being that implements iMovement.
 */
abstract class Being implements iMovement{

	private $isAlive = true;

        /**
         * Gets the isAlive
         * @return type boolean
         */
	public function isAlive(){
		
            return $this->isAlive;
		
	}
        
        /**
         * Sets the isAlive to true while displaying the string
         * @return string
         */
        public function alive(){
		$this->isAlive = true;
                return 'Alive!<br/>';
	}

        /**
         * Sets the isAlive to false while displaying the string
         * @return string
         */
	public function kill(){
		$this->isAlive = false;
                return 'Dead!<br/>';
	}
	
}


