<?php
/**
 * A Simple OOP example in PHP.
 * This example uses NAMESPACE as well.
 * 
 * @author Niraj Byanjankar <nirajbjk@gmail.com>
 * @copyright (c) 2015, Niraj Byanjankar
 */
require_once 'Cat.php';

use Being\Animal\Cat\Cat as Cat;

$cat = new Cat("Cici",4);
//$cat->isAlive();
echo $cat->getName() . " is " . $cat->getAge() . " years old.<br/>";
echo $cat->canWalk();
echo $cat->canRun();
echo $cat->getName() . ' is ' . $cat->kill();
echo $cat->canWalk();
echo $cat->canRun();

echo $cat->getName() . ' is ' . $cat->alive();
echo $cat->canWalk();
echo $cat->canRun();

