<?php
namespace Being\Animal\Cat;
/**
 * This is a tester page for Cat class using PHPUnit Test.
 * @author      Niraj Byanjankar <nirajbjk@gmail.com>
 * @copyright (c) 2015, Niraj Byanjankar
 * @example phpunit --bootstrap src/being.php tests/catTest.php
 */

class CatTest extends \PHPUnit_Framework_TestCase {

        /**
         * Test Cat Alive
         */
	public function testCatAlive(){
		$cat = new Cat("Cici",3);
		
		
		$this->assertEquals(true,$cat->isAlive());
	}
	
        /**
         * Test Function exists?
         * 
         */
	public function testFunction(){
	
		$this->assertEquals(1,new Cat("Mimi",2));
	}

    
}